\documentclass{llncs}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath,array,booktabs}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[numbers,sort&compress]{natbib}
%\usepackage[pdftex]{graphicx}
\usepackage{url}


\renewcommand{\bibsection}{\section*{References}}



\newcommand\Ende{En$\rightarrow$De}
\newcommand\ensv{En$\rightarrow$Sv}
\newcommand\enel{En$\rightarrow$El}
\newcommand\enes{En$\rightarrow$Es}
\newcommand\enit{En$\rightarrow$It}
\newcommand\enfr{En$\rightarrow$Fr}

\title{eTranslation's Submissions to the COVID19-MLIA Translation Task}
\author{Csaba Oravecz$^\dagger$ 
   Katina Bontcheva$^\dagger$ 
   David Kolovratn\'ik$^\dagger$
  Bogomil Kovachev$^\star$
  Vilmantas  Liubinas$^\star$
  Christopher Scott$^\star$
   Francois Thunus$^\star$
  Andreas Eisele$^\star$}
\institute{DG Translation -- DG CNECT, European Commission\\
$^\dagger$\texttt{firstname.lastname@ext.ec.europa.eu}\\
$^\star$\texttt{firstname.lastname@ec.europa.eu}}
\begin{document}
\maketitle

\begin{abstract}
  The report describes the 6 NMT models submitted by the eTranslation
  team to the COVID19-MLIA translation shared task in Round 2. We
  developed systems in language pairs that are actively used in the
  European Commission's eTranslation service. We focused primarily on
  data filtering and applied standard techniques of fine tuning and
  ensembling to improve our models. The submitted systems scored
  competitively in all language pairs, and several of our models were
  the best according to the automatic evaluation.
\end{abstract}

\section{Introduction}
\label{sec:introduction}

The eTranslation team submitted constrained and unconstrained systems
for 6 language pairs. We used standard best practices to develop the
models focusing on cleaning up the provided training data and finding
optimal architectures for training. For the unconstrained models we
generally tried to use all available health related data that we had
access to, which in some language pairs led to a significant increase
in model quality while in other cases the difference in performance
was not substantial. The systems submitted and the experiments during
the development are described in detail for each language pair in the
following sections.

\section{English$\rightarrow$German}
\label{sec:engl-right-germ}

\subsection{Data}
\label{sec:data}

For the second round of submissions the provided parallel data increased
considerably. At the same time significant sections of the data set could
hardly be considered clean parallel data, only comparable. Therefore we
applied some basic rule based filtering as well as some simple
heuristics to exclude noisy segments. 
As a general clean-up, we performed the following steps on the raw data set:
\begin{itemize}
\item language identification with FastText\footnote{\texttt{https://fasttext.cc/docs/en/language-identification.html}} \citep{joulin2016fasttext},
\item deletion of segments where source/target token ratio exceeds 1:3 (or 3:1),
\item deletion of segments longer than 110 tokens,
\item exclusion of segments where the ratio between the number of
  characters and the number of words was below 1.5 or above 40,
\item exclusion of segments without a minimum number (4) of alphabetic
  characters.
\end{itemize}

These filtering steps led to a 3\% reduction of the data set (from 2,462,556
segments to 2,392,900).

Using some more elaborate approach based on powerful resources to remove
segments where the target side was far from being the translation of the
source was not a viable option for the constrained category, therefore we
applied only a basic heuristic: if there was a mismatch in the numeric
tokens\footnote{When checking mismatch, we removed all punctuations (eg.
decimal comma or dot, slash or hyphen etc.) from the numerals.} between source
and target we removed the segment from the data set. This resulted in a
further 6\% reduction (from 2,392,900 to 2,249,452 segments). We did not perform a
methodical evaluation on the performance of this filter but on a small scale
manual evaluation its precision was very high, segments marked for removal
were indeed non-parallel. Recall however was probably much lower, many noisy
segments could still remain in the data set. However, we did not perform more
filtering and used this set of 2,249,452 segments to build our models.

To test our models we used several different test sets from the data
available, including a post submission in house test set created from
health related Euramis segments (see below) and a 2k test set
extracted from Round 2 validation data. Results were not homogeneous
across models and test sets, some models performed better on one set
but worse on another. Models selected for submission performed strong
on the development test set so we mainly report results for these models.


\subsection{Training}
\label{sec:training}

Similarly to Round 1, for the constrained experiments, we first built
a base transformer model from the filtered training data. The training
process was also similar, we split up the validation set into two
halves and used one part to stop the trainings if sentence-wise
normalized cross-entropy on the validation set did not improve in 5
consecutive validation steps, and reserved the other part as a test
set. We did not apply any standard pre- and postprocessing steps of
truecasing, or (de)tokenization; we simply used SentencePiece
\cite{P18-1007}, which allows raw text input/output within the Marian
toolkit \cite{P18-4020}\footnote{We used default settings for Marian's
  built-in SentencePiece: unigram model, built-in normalization and no
  subword regularization.} For most of the hyperparameters we used the
default settings for the base transformer architecture in
Marian\footnote{See eg.
  \texttt{https://github.com/marian-nmt/marian-examples/tree/master/transformer}.}
with dynamic batching and tying all embeddings.  We experimented
with big transformer architectures, here we also followed recommended
settings for Marian, we doubled the filter size and the number of
heads, decreased the learning rate from 0.0003 to 0.0002 and halved
the update value for \texttt{--lr-warmup} and
\texttt{--lr-decay-inv-sqrt}.  We experimented with (joint) vocabulary
sizes of 12k and 32k, the latter resulting in somewhat better
scores. The submissions models used the latter setting. As a last step
in the training, our best constrained model benefited from a 5 epoch
fine-tuning on the Round 1 and 2 validation and Round 1 test sets.

The trainings were run as multi-GPU trainings on 2 (base transformer) or 4
(big transformer) NVIDIA V100 GPUs with 16GB RAM, for around 30 epochs. 

\subsection{Unconstrained models}
\label{sec:unconstrained}

Our first unconstrained submission is a single big transformer model trained
from the filtered training set extended with the TAUS Corona Crisis
Corpora\footnote{\texttt{https://md.taus.net/corona}} (610k segments), the
OPUS EMEA Corpus\footnote{\texttt{https://opus.nlpl.eu/EMEA.php}} (760k
segments), and a health related subset of the Euramis data set
\citep{Steinberger2014} (1.1M segments). 

The second unconstrained submission as in Round 1 is based on and uses
the strongest model that the eTranslation team submitted to the WMT
2021 News Task. This model is a 4 member big transformer ensemble,
trained as a constrained submission for WMT on more than 400M original
parallel and back-translated segments, with fine tuning on the
development sets.  Looking at the result of Round 2 we can confirm
that what we hypothesized in Round 1 still holds: this model, although
primarily oriented towards the news domain, is a powerful general MT
system, and it significantly outperforms all other systems in the
current task already in zero shot mode. In the submission model, each
of the 4 big transformer models is fine tuned on the filtered training
data for 3 epochs and ensembled together with equal weights. This
model is marginally better than the zero shot variant. This and the
large difference between the constrained and unconstrained models seem
to suggest that the big WMT model already gives good support for the
test set in the current task, and, possibly, the test set is still
closer to the news domain in general than to the range of the
(in-domain) training data (which itself might still be heterogeneous
and as such too small to compete with the large general news model).

\subsection{Results}
\label{sec:results}

\begin{table}[ht]
  \centering
  \begin{tabular}{lcccc}
    \toprule
    & & \multicolumn{3}{c}{Test sets} \\ \cmidrule(l){3-5}
    System & Data & Euramis (2k) & R2-V (2k)& R2 off. (4k)\\\midrule
    Round 1 (c) & 926k  & 32.7 & 29.2 & 27.7 \\
    Round 2 raw (c) & 2.46M & 35.3  &39.9 &39.7  \\
    Round 2 filt. (c) & 2.25M &35.9 &40.3 &39.7 \\
    R 2 filt. big Tr. (c)& 2.25M &35.3 & 39.0 & 38.3\\
    Big tr. ensemble* (c) & 2.25M &37.3 &41.7 & 40.9 \\
    Big tr. ens. fine t.* (c) & 2.25M+6.5k & 37.3& -- & \textbf{41.1}\\\midrule
    Single big Tr.* (u) & 3.89M & -- &41.3 & 41.2 \\
    WMT21 (u) & 430M &38.7 &45.9 & 46.2\\
    WMT21 fine t.* (u) & 430+2.25M &39.9 &47.4 & \textbf{47.1}\\
    \bottomrule
  \end{tabular}
  \caption{\label{tab:enderesults} Results for \Ende\ models. R2-V is
    the development test set extracted from Round 2 validation
    data. (c): constrained, (u): unconstrained model. Submissions are
    marked with an asterisk.}
\end{table}

In Table~\ref{tab:enderesults} we present the BLEU
scores\footnote{sacreBLEU signatures:
  \texttt{BLEU+case.mixed+lang.en-de+numrefs.1+smooth.exp+tok.13a+\\
    version.1.5.1}} for the main models we experimented with in the
development of \Ende\ systems.  Models marked by an asterisk are
submission models. We do not calculate the scores where the training
data already contains the test segments of the particular test
set. Results reported with the Round 2 test set (last column) for the
submitted models are slightly higher than on the evaluation
portal. The reason for this is that we submitted versions of the
models' hypotheses with a postprocessing step normalizing German
punctuation but as it turned out the reference set did not follow
German typography, so to calculate the scores in
Table~\ref{tab:enderesults} we rolled back to the raw model
outputs. According to these results we achieved the highest scores in
both the constrained and unconstrained categories.

The different test sets do not seem to give a clear, unanimous
indication of the best workflows and model architectures, the various
setups behave slightly differently on one or the other test set. From
the development test sets it seems that data filtering was a useful
step while the Round 2 test set gives identical scores for this
setting. The switch from the base to the big transformer architectures
was not, according to the scores, justified, although the ensemble
clearly outperformed all other (constrained) models. It has been shown
that in low resource scenarios moderate architectures perform better
\citep{DBLP:journals/corr/abs-2004-04418}, and it seems even in medium
resource settings the base transformer architecture remains
competitive. Fine tuning with the development sets in the constrained
settings yielded a small increase but in the unconstrained models the
same technique using the full in-domain training set led to a more
significant improvement.

\section{English$\rightarrow$Swedish}
\label{sec:english-swedish}

\subsection{Data}
\label{sec:data-1}

Training data was made up of segments from both Round 1 and Round 2,
while validation data only came from what was provided for Round
2. After sampling the data manually for quality we performed two clean
up tasks to filter out certain problematic segments. The nature of the
crawled data meant that in many cases, numbers and locations did not
match between segments and these were removed where possible using
scripts. This reduced the data volume by approximately 25\%.
Our sole test set was created using data from Rounds 1 and 2 and
contained approximately 9000 segments.

\subsection{Training}
\label{sec:training-1}

We began by training a base transformer model. We used the same early
stopping strategy as in the \Ende{} training and the same raw
SentencePiece tokenization method.  We then moved on to experiments
using big transformer models. Here we also followed recommended
settings for Marian as used in the \Ende{} language pair. Our
vocabulary size for both architectures in contrast was fixed for all
cases at 36k. Our best result came from an ensemble of 4
models.  The trainings were run on the same environments as the
other language pairs, for less than 20 epochs for the base and
70 for the big transformer.

\subsection{Unconstrained models}
\label{sec:unconstrained-models}

For the unconstrained models, the data initially included only some
additional Euramis data related to the medical domain. To this data we
tried to add segments from multiple sources --- general OPUS and
ParaCrawl data filtered on basic medical terms, the OPUS EMEA Corpus,
and additional in house public health data. We again experimented
with base and big transformer architectures, and again the best result
came from a 4 model big transformer ensemble.

\subsection{Results}
\label{sec:results-1}

\begin{table}[ht]
  \centering
  \begin{tabular}{lccc}
    \toprule
    & & \multicolumn{2}{c}{Test sets} \\ \cmidrule(l){3-4}
    System & Data & R1+R2 (9k)& R2 off. (4k)\\\midrule
    Base Tr. (c) & 900k  & 51.8 &  20.3 \\
    Big Tr. (c) & 900k & 54.3  & 20.0  \\
    Big Tr. Ensemble (c) & 900k & 56.3 & \textbf{22.7} \\\midrule
    Base Tr. Euramis (u) & 1.75M & 52.2 & 20.9 \\
    Base Tr. multi (u) & 2.5M &53.9 & 22.0\\
    Big Tr. multi ensemble (u) & 2.5M & 56.6 & \textbf{23.3} \\
    \bottomrule
  \end{tabular}
  \caption{\label{tab:ensvresults} Results for \ensv\
    models. \emph{multi} is the multiple source data described
    above. R1+R2 is the development test set we created ourselves from data from
    Round 1 and Round 2. (c): constrained, (u): unconstrained model.}
\end{table}

In Table~\ref{tab:ensvresults} we present the BLEU scores for the main models we
experimented with in the development of \ensv{} systems. All models used
filtered data. Despite the relatively low number of segments in the training data
($<$ 1M), using a big transformer architecture here led to noticeably
improved BLEU scores for both the constrained and unconstrained
models. With our own test set, each addition of more data as well as
passing from base to big to an ensemble architecture systematically
increases the score. Results from the official test set show almost
exactly the same behaviour apart from the base to big transformer
where the trend is reversed. However, the big transformer ensemble
shows an improvement of 2 points compared to the base
architecture. The BLEU scores resulting from our own test set and the
official test set are however vastly different. One reason may be that
our own test set was filtered to some extent, though this alone would
not explain such disparities.

\section{English$\rightarrow$Greek}
\label{sec:enel}

\subsection{Data}
\label{sec:data-2}

For the constrained task we used the data sets distributed for Round 1
and Round 2. For the unconstrained task training data from Euramis was
added, similarly to the other language pairs. For both the constrained
and unconstrained tasks we extracted validation and test data sets
from Round 1 and Round 2 data. The number of segments in the data sets
is summarized in Table~\ref{tab:eneldata}.

\begin{table}[ht]
  \centering
  \begin{tabular}{lccc}
    \toprule
    & \multicolumn{1}{c}{Training} & \multicolumn{1}{c}{Validation} & \multicolumn{1}{c}{Testing}\\\midrule
    Constrained & 1,315,111 & 4,000 & 10,000 \\
    Unconstrained & 2,259,617 & 4,000 & 10,000 \\
    \bottomrule
  \end{tabular}
  \caption{\label{tab:eneldata} Number of segments used to train the \enel{} models.}
\end{table}

There was no pre- or postprocessing of the data, although it was
checked for sanity and consistency.

\subsection{Models}
\label{sec:models}

For each of the two tasks (constrained and unconstrained) we trained
(A) a base transformer (with default settings in the Marian toolkit)
and (B) a big transformer (again with standard settings, see
Section~\ref{sec:training}). We used a vocabulary size of 36k
uniformly. Each model was trained on 4 GPUs.

\subsection{Results}
\label{sec:results-2}

For \enel{}, we submitted the raw model output without any
postprocessing. The BLEU scores are summarized in Table~\ref{tab:enelresults}.

\begin{table}[ht]
  \centering
  \begin{tabular}{lcc}
    \toprule
     & \multicolumn{2}{c}{Test sets} \\ \cmidrule(l){2-3}
    System & R1+R2 dev (10k)& R2 off. (4k)\\\midrule
    Constrained Base Tr.  & 47.9  & 41.7\\
    Constrained Big Tr. & 47.9 & 34.9\\\midrule
    Unconstrained Base Tr. & 48.3 & 44.3\\
    Unconstrained Big Tr. & 48.3 & 43.1 \\
    \bottomrule
  \end{tabular}
  \caption{\label{tab:enelresults} Results for \enel\ models.}
\end{table}

\section{English$\rightarrow$Spanish}
\label{sec:english-spanish}


The \enes{} workflow was similar to the \enel{} one, except that the
validation set was solely from Round 2 data.
The data statistics are summarized in Table~\ref{tab:enesdata}.

\begin{table}[ht]
  \centering
  \begin{tabular}{lccc}
    \toprule
    & \multicolumn{1}{c}{Training} & \multicolumn{1}{c}{Validation} & \multicolumn{1}{c}{Testing}\\\midrule
    Constrained & 3,428,760 & 4,000 & 10,000 \\
    Unconstrained & 4,518,984 & 4,000 & 10,000 \\
    \bottomrule
  \end{tabular}
  \caption{\label{tab:enesdata} Number of segments used to train the \enes{} models.}
\end{table}

\subsection{Trainings and results}
\label{sec:trainings-results}

We experimented with the same setups as in \enel{}. Interestingly, our
submitted systems scored much more competitively in this language pair
than in \enel{}, our unconstrained system being the best by a
significant margin. The results are summarized in Table~\ref{tab:enesresults}.

\begin{table}[ht]
  \centering
  \begin{tabular}{lcc}
    \toprule
     & \multicolumn{2}{c}{Test sets} \\ \cmidrule(l){2-3}
    System & R1+R2 dev (10k)& R2 off. (4k)\\\midrule
    Constrained Base Tr.  & 46.2  & 56.1\\
    Constrained Big Tr. & 46.6 & 56.1\\\midrule
    Unconstrained Base Tr. & 45.8 & 56.0\\
    Unconstrained Big Tr. & 46.4 & 56.5 \\
    \bottomrule
  \end{tabular}
  \caption{\label{tab:enesresults} Results for \enes{} models.}
\end{table}


\section{English$\rightarrow$ Italian}
\label{sec:english-italian}

\subsection{Data}
\label{sec:data-3}

For the constrained task, we used the training data provided for Round
1 and Round 2. It was cleaned and filtered the same way as the data
for \Ende{}. The development set was used for validation and the test
data was extracted at random from the cleaned and filtered training
data (\emph{dev\_test\_set}). A subset of the training data was
extracted using a few keyword patterns, and this subset was used for
fine-tuning.

For the unconstrained task, we experimented with adding data from the
from Euramis and other sources, similarly to \Ende{}. First, we
extracted Euramis documents that, based on the metadata, were in-domain
(\emph{mtdata1}). The next addition were segments from the same database but
extracted using keywords (\emph{mtdata2}). The last addition was a
combination of data from EMEA, TAUS Corona Crisis Corpora, and
proprietary public-health data (\emph{var\_med}).

The number of segments in the training data is presented in
Table~\ref{tab:enitdata}.

\begin{table}[ht]
  \centering
  \begin{tabular}{lcccc}
    \toprule
   &\multicolumn{1}{c}{Composition}  & \multicolumn{1}{c}{Training} & \multicolumn{1}{c}{Validation} & \multicolumn{1}{c}{Testing}\\\midrule
    Constrained & R1+R2& 1.6M & 4,000 & 10,000 \\
    Constrained subset for FT & R1+R2 & 100k & 4,000& 10,000 \\
    Unconstrained & R1+R2+mtdata1& 2.5M &4,000& 10,000\\ 
    Unconstrained & R1+R2+mtdata\{1,2\}+var\_med& 3.7M & 4,000 & 10,000 \\
    \bottomrule
  \end{tabular}
  \caption{\label{tab:enitdata} Number of segments used to train the
    \enit{} models. FT: fine-tuning.}
\end{table}

\subsection{Models and results}
\label{sec:models-1}

We built our baseline models for the constrained task as a standard
base transformer model. Then we switched to a standard big transformer
model. Training parameters were similar to \Ende{}. Since the big
transformer model gave better scores, we built 3 more for a 4 member
ensemble. Finally, we fine-tuned all 4 big transformers.

For the unconstrained task, we experimented with a big transformer
using \emph{R1+R2+mtdata1}, a base transformer, and a 4 big
transformer ensemble, using \emph{R1+R2+mtdata1+mtdata2+var\_med}.  We
did not normalize the translations, except in one case
(cf. Table~\ref{tab:enitresults}). The normalized translation got a
slightly lower score.

We present the \enit{} results in Table~\ref{tab:enitresults}.  We
did not expect that for such a small amount of training data
(1.6--3.7M) the big transformer would be beneficial for the results
but in this case it yielded better scores. According to the automatic
evaluation, our unconstrained submission was the strongest system in this
language pair.

\begin{table}[ht]
  \centering
  \begin{tabular}{lccc}
    \toprule
     & \multicolumn{2}{c}{Test sets} \\ \cmidrule(l){3-4}
    System & Data & R1+R2 dev (10k)& R2 off. (4k)\\\midrule
    Base Tr. (c) & R1+R2 & 43.3  & 45.1\\
    Big Tr. (c) & R1+R2& 46.3 & 44.0\\
    Big tr. ens.* (c) & R1+R2& 47.7& 47.0\\
    Big tr. ens. FT* (c) & R1+R2& 47.8 & 46.7 \\\midrule
    Big tr. (u)&R1+R2+mtdata1  & 46.3 & 46.6\\
    Base tr. (u) & R1+R2+mtdata1+mtdata2+var\_med & 45.9 & 46.8 \\
    Big tr. (u) & R1+R2+mtdata1+mtdata2+var\_med & 48.5 & 48.3 \\
    Big tr. ens.* (u) & R1+R2+mtdata1+mtdata2+var\_med & 49.4 & 50.1 (49.9) \\
    \bottomrule
  \end{tabular}
  \caption{\label{tab:enitresults} Results for \enit{} models. All
    scores are for non-normalized translations, except the score in
    parentheses; (c): constrained, (u): unconstrained
    model. Submissions are marked with an asterisk.}
\end{table}


\section{English$\rightarrow$French}
\label{sec:english-french}

For the constrained \enfr{} system we used the all the provided data
with some minimal filtering (see Section~\ref{sec:data}). Our
unconstrained submission, not specifically trained for this task, was
the stock eTranslation general engine (trained from Euramis and OPUS
data) with normalized and raw outputs.

Data is summarized in Table~\ref{tab:enfrdata}.

\begin{table}[ht]
  \centering
  \begin{tabular}{lccc}
    \toprule
    & \multicolumn{1}{c}{Training} & \multicolumn{1}{c}{Validation} & \multicolumn{1}{c}{Testing}\\\midrule
    Constrained & 2.9M & 4,000 & 10,000 \\
    Unconstrained & 237M & -- & -- \\
    \bottomrule
  \end{tabular}
  \caption{\label{tab:enfrdata} Number of segments used to train the \enfr{} models}
\end{table}

\subsection{Trainings and results}
\label{sec:trainings-results}

We experimented with the same constrained setups as in \enel{} or
\enes{}. These systems were the winning submissions for this language
pair outperforming the unconstrained submissions as well, while in the
unconstrained submissions, it is worth noting that, similarly to
\Ende{}, the normalization of punctuation in postprocessing proved to
make a significant (here 7 BLEU points!) difference. This might
suggest that it would be beneficial to ensure some standardization in
the reference sets with respect to typography to get a more reliable
indication of the translation quality. The results are summarized in
Table~\ref{tab:enfrresults}.

\begin{table}[ht]
  \centering
  \begin{tabular}{lcc}
    \toprule
     & \multicolumn{2}{c}{Test sets} \\ \cmidrule(l){2-3}
    System & R1+R2 dev (10k)& R2 off. (4k)\\\midrule
    Constrained base tr. & 47.0 & 57.9\\
    Constrained big tr. & 47.6 & 58.3\\\midrule
    Unconstrained normalized & -- & 49.9\\
    Unconstrained raw & -- & 56.9 \\
    \bottomrule
  \end{tabular}
  \caption{\label{tab:enfrresults} Results for \enfr{} models.}
\end{table}



\section{Conclusions}
\label{sec:conclusions}

We described the submissions of the eTranslation team to the second
round of the COVID19-MLIA translation shared task on 6 language pairs.
Compared to Round 1, our systems were more competitive ending up in
first place in several categories.  We tried to focus on data
selection and filtering and experimented with some complex
architectures, and automatic evaluation results justified this
approach. We hypothesize that when similar development workflows
resulted in worse positions in the rankings for different language
pairs, the diversity and noise in the data sets might have played a
role: some of our models performed accidentally better or worse on the
specific datasets but the general performance of these models are most
likely similar. This hypothesis has some support from the strong
performance of the most powerful unconstrained systems, which is
probably due to their robustness.  However, further testing would be
necessary to confirm these assumptions.

\bibliographystyle{splncs04nat}
\bibliography{anthology,wmt2021,mt,etranslation}

\end{document}

